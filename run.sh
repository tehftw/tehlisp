#!/bin/sh

clear &&
	gcc -Werror -fmax-errors=1 -Wall -Wextra -g -Og -rdynamic -lSegFault src/*.c -o bin/gcc-teh-lisp &&
	./bin/gcc-teh-lisp txt/ex.t
