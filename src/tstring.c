#include "tstring.h"

struct tstring * tstring_new_with_size( size_t const size ) {
	struct tstring * tstr = teh_calloc( 1, sizeof(struct tstring) );
	tstr->size = size;
	tstr->length = 0;
	tstr->c = teh_calloc( size + 1 , sizeof(char) );
	return tstr;
}

void tstring_resize( struct tstring * tstr , size_t const newsize )
{
	teh_assert( tstr );
	tstr->c = teh_realloc( tstr->c , ((1 + newsize) * sizeof(char)) );
	tstr->size = newsize;
}

void tstring_delete( struct tstring * tstr )
{
	free( tstr->c );
	free( tstr );
}

void tstring_push_char( struct tstring * tstr , char  ch )
{
	teh_assert( tstr );
	if( !(tstr->size > tstr->length ) ) {
		tstring_resize( tstr , (2 * tstr->size)  );
	}
	tstr->c[tstr->length] = ch;
	++(tstr->length);
}



