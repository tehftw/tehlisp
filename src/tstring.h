#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include <setjmp.h>

#include "teh-alloc.h"
#include "exception.h"

struct tstring {
	size_t size;
	size_t length;
	char * c;
};
