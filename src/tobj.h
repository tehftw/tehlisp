#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <setjmp.h>
#include <string.h>
#include <stdarg.h>
#include <stdbool.h>
#include <ctype.h>


#include "exception.h"
#include "teh-alloc.h"
#include "macros.h"



enum type {
	 type_double
	,type_int
	,type_arr_char
	,type_pair
	,type_pointer
	,type_arr_tobj
	,type_special
	,type_lambda
	,type_primitive
	,TYPES_COUNT
};
#define type_char type_arr_char


typedef struct tobj Tobj;
typedef struct tobj * tobj_ptr;


struct tobj {
	enum type type;
	size_t capacity; /* for arrays: how many records of the current type can be stored*/
	size_t length; /* length of array - number of entries in a stack */
	
	bool is_symbol;
	
	void * ptr;
	union {
		void * void_ptr;
		int val_int; /* type_int */
		double val_double; /* type_double */
		char val_char[sizeof(struct tobj *[2])]; /* very short string literal, designed for special symbols 't' and 'f' */
		char * arr_char; /* type_arr_char */
		struct tobj * pair[2]; /* type_pair */
		struct tobj * pointer;	/* type_pointer */
		struct tobj * arr_tobj; /* type_arr_tarr */
		struct {
			int min_args;
			int max_args;
			struct tobj * body;
		} lambda;
		struct { /* type_primitive */
			char * name;
			// enum primitive_code code
			int code;
		} primitive;
	};
};

extern struct tobj * f;
extern struct tobj * t;


bool is_valid_type( enum type const type );
size_t get_size_of_member( enum type const type );


bool tobj_has_array( const struct tobj * const obj );


void exception_with_tobj( struct tobj const * tobj , char *format , ... )
  __attribute__ ((noreturn, format(printf, 2, 3)));




void tobj_print( FILE * out , struct tobj const * tobj , bool const readably );
void tobj_print_2( FILE * out , struct tobj const * tobj , bool const readably );




bool tobj_is_array( struct tobj const * const obj ) ;
struct tobj * tobj_alloc( /* allocates memory for 'struct tobj' + 'size' bytes of memory */ 
		size_t const size ) ;
struct tobj * tobj_new( enum type const type ) ;
struct tobj * tobj_new_with_array( enum type const type ,  size_t const ) ;

void tobj_resize_array( struct tobj * obj , size_t const size ) ;
void tobj_autogrow_array( struct tobj * obj );
void tobj_resize_array_to_fit( struct tobj * obj );

void tobj_delete( struct tobj * obj ) ;









struct tobj * tobj_new_arr_char_size( size_t const size ) __attribute__ ((deprecated));
struct tobj * tobj_new_arr_char( size_t const ) ;
struct tobj * tobj_new_arr_char_copy_cstring( const char * cstr  ) ;
void tobj_arr_char_push( struct tobj * obj ,  const char c ) ;

struct tobj * tobj_new_symbol_copy_cstring( const char * cstr  ) ;

struct tobj * tobj_new_arr_tobj( size_t const );



struct tobj * tobj_new_pointer( struct tobj * ptr) ;
struct tobj * tobj_new_double( const double val ) ;
struct tobj * tobj_new_int( const int val ) ;
struct tobj * tobj_new_pair( struct tobj * ptr0 , struct tobj * ptr1 ) ;

struct tobj * tobj_pair_get_head(struct tobj * obj) ;
struct tobj * tobj_pair_get_tail(struct tobj * obj) ;
struct tobj * tobj_pair_insert_after(
/* returns pointer to the pair containing next */
		struct tobj * list , struct tobj * obj  ) ;
struct tobj * tobj_pair_insert_after_1(
/* returns pointer to the pair containing next */
		struct tobj * list , struct tobj * obj  ) ;

void tobj_pair_extend(
/*
   if (*ref == NULL): constructs a list with obj as car
   if (*ref->type == type_pair): appends obj as new cell, after *ref
*/
		 struct tobj ** ref
		,struct tobj * obj  ) ;

struct tobj * tobj_copy( const struct tobj * obj ) ;
int tobj_get_int( struct tobj * obj ) ;
double tobj_get_double( struct tobj * obj ) ;
bool tobj_are_equal( struct tobj * t_0 , struct tobj * t_1 ) ;

