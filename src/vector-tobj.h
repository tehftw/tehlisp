#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <setjmp.h>
#include <string.h>
#include <stdarg.h>
#include <stdbool.h>
#include <ctype.h>


#include "exception.h"
#include "teh-alloc.h"
#include "macros.h"
#include "tobj.h"



struct vector_tobj {
	size_t capacity;
	size_t length;
	tobj_ptr * arr_tobj;
};



void vector_tobj_initialize( struct vector_tobj * vect , size_t const capacity );
struct vector_tobj * new_vector_tobj( size_t const capacity );
void delete_vector_tobj( struct vector_tobj ** vect);
void vector_tobj_reserve( struct vector_tobj * vect , size_t const capacity );
void vector_tobj_push( struct vector_tobj * vect , struct tobj * obj );
struct tobj * vector_tobj_pop( struct vector_tobj * vect );
struct tobj ** vector_tobj_ref_top( struct vector_tobj * vect );
struct tobj * vector_tobj_peek( struct vector_tobj * vect );
void vector_tobj_print_content( FILE * out , struct vector_tobj * vect );
