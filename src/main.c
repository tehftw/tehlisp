



#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <setjmp.h>
#include <string.h>
#include <stdarg.h>
#include <stdbool.h>
#include <ctype.h>

#include <unistd.h>
#include <signal.h>
#include <execinfo.h>

#include "exception.h"
#include "teh-alloc.h"
#include "macros.h"
#include "tobj.h"
#include "vector-tobj.h"
#include "parse-stream.h"
#include "environment.h"
#include "evaluate.h"




/*
TODO:
1. add checking whether the list is ??? the fuck I wanted to write here?!??!?!
3. add lambdas
4. LAMBDAS! procedures, functions etc!
5. add a (format ) procedure
6. memory management


Parsing/reading:
1. make a new, non-recursive parser
2. comments
3. counting lines while reading file
. add string-handling to parser DONE
*/






void test_match_primitive(void)
{
	printf( "\n{ <TEST-match_primitive_by_symbol>\n" );
	struct tobj * head = tobj_new_pair( 0 , 0 );
	{
		struct tobj * last = head;
		last = tobj_pair_insert_after( last , evaluate( tobj_new_symbol_copy_cstring("list") ) );
		last = tobj_pair_insert_after( last , evaluate( tobj_new_symbol_copy_cstring("abc") ) );
		last = tobj_pair_insert_after( last , evaluate( tobj_new_symbol_copy_cstring("0x14") ) );
		tobj_print( stdout , head , true);
	}
	
	tobj_print( stdout , evaluate( head ) , true);
	printf( "\n}\n" );
}


void test_parse_stream_recursive( char const * filepath )
{
	teh_assert( filepath != NULL);
	FILE * in = fopen( filepath , "r" );
	teh_assert( in != NULL );
	
	struct tobj * parsed = parse_stream_recursive_2( in );
	tobj_print( stdout , parsed , true );
	fclose( in );
	
	
	
	printf( "finished parsing file `%s`" , filepath );
	printf( "\n - - - \n" );
	struct tobj * evaled_head = tobj_new_pair( 0 , 0 );
	struct tobj * evaled_node = evaled_head;
	for( struct tobj * node = parsed; node != NULL; node = node->pair[1]  ) {
		evaled_node = tobj_pair_insert_after( evaled_node , evaluate( node->pair[0] ));
	}
	
	printf( "\n" );
	for( struct tobj * node = evaled_head; node != NULL; node = node->pair[1] ) {
		tobj_print( stdout , node->pair[0] , true );
		printf( "\n" );
	}
	
	
	printf( "\n - - - \nfinished\n - - - \n" );
	env_stack_print( stdout , ENV );
}


void test_parse_stream_recursive_istream( char * filepath )
{
	teh_assert( filepath != NULL);
	Istream * in = istream_open_file( filepath );
	teh_assert( in != NULL );
	
	struct tobj * parsed = istream_parse_stream_recursive( in );
	tobj_print( stdout , parsed , true );
	istream_close( in );
	
	
	
	printf( "finished parsing file `%s`" , filepath );
	printf( "\n - - - \n" );
	struct tobj * evaled_head = tobj_new_pair( 0 , 0 );
	struct tobj * evaled_node = evaled_head;
	for( struct tobj * node = parsed; node != NULL; node = node->pair[1]  ) {
		evaled_node = tobj_pair_insert_after( evaled_node , evaluate( node->pair[0] ));
	}
	
	printf( "\n" );
	for( struct tobj * node = evaled_head; node != NULL; node = node->pair[1] ) {
		tobj_print( stdout , node->pair[0] , false );
		printf( "\n" );
	}
	
	
	printf( "\n - - - \nfinished\n - - - \n" );
	env_stack_print( stdout , ENV );
}


int main( int argc , char *argv[] )
{
	printf( "\n%d   %p\n" , SIGSEGV , exit_with_backtrace );
	
	putchar( '\n' );
	
	signal( SIGSEGV , exit_with_backtrace);
	
	if( argc == 1 ) {
		printf( "usage: %s <file>" , argv[0]  );
		exit(EXIT_SUCCESS);
	}
	
	int exception_code;
	if( (exception_code = setjmp(exception_env)) ) {
		fprintf( stderr , "an exception has occured, exiting...\n" );
		exit(EXIT_FAILURE);
	}
	
	
	struct env main_env;
	ENV = &main_env;
	env_initialize( ENV  , 0x10 );
	
	
	if( argc == 2 ) {
		test_parse_stream_recursive_istream( argv[1] );
		exit( EXIT_SUCCESS );
	}
	
	putchar( '\n' );
}


void test_equal(void) {
	printf( "\ntest are_equal: %d\n" , tobj_are_equal( tobj_new_int(2) , tobj_new_int(1) ) );
	printf( "\ntest are_equal: %d\n" , tobj_are_equal( tobj_new_int(2) , tobj_new_double(2) ) );
	
	tobj_print( stdout , 0 , true );
	tobj_print( stdout , tobj_new_pair( 0 , 0) , true );
}


