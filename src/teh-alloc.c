#include "teh-alloc.h"




void * teh_malloc( size_t const size )
{
	void * ptr = malloc( size );
	teh_assert( ptr != NULL );
	if( ptr == NULL ) {
		exception( "failed to allocate %#zx bytes" , size );
	}
	return ptr;
}
void * teh_calloc( size_t const s0 , size_t const s1 )
{
	teh_assert( s0 > 0 );
	teh_assert( s1 > 0 );
	void * ptr = calloc( s0 , s1 );
	teh_assert( ptr != NULL );
	if( ptr == NULL ) {
		exception( "failed to calloc(%#zx , %#zx)" , s0 , s1 );
	}
	return ptr;
}
void * teh_realloc( void * ptr_r, size_t const size )
{
	teh_assert( ptr_r != NULL );
	teh_assert( size > 0 );
	void * ptr = realloc( ptr_r , size );
	teh_assert( ptr != NULL );
	if( ptr == NULL ) {
		exception( "failed to realloc(%p, %#zx)" , ptr_r , size );
	}
	return ptr;
}
