#include "evaluate.h"




/* { primitives */

struct tobj * evaluate( struct tobj * expr );


struct tobj * primitive_procedure_quote( struct vector_tobj * args) {
	teh_assert( args != NULL );
	teh_assert( args->length > 0 );
	teh_assert( args->length < 2 );
	// TODO: make it work
	
	return args->arr_tobj[0];
}
struct tobj * primitive_procedure_eval( struct vector_tobj * args) {
/* relies on a special form in evaluate(): so that args->arr_tobj[0] isn't evaluated, but rather passed on as a list */
	teh_assert( args != NULL );
	teh_assert( args->length == 1 );
	teh_assert( args->arr_tobj[0]->type == type_pair );
	// TODO: make it work
	return evaluate( args->arr_tobj[0] );
}
struct tobj * primitive_procedure_print( struct vector_tobj * args) {
	teh_assert( args != NULL );
	teh_assert( args->length > 0 );
	teh_assert( args->length < 2 );
	// TODO: make it work
	
	tobj_print( stdout , args->arr_tobj[0] , true );
	
	return 0;
}
struct tobj * primitive_procedure_display( struct vector_tobj * args) {
	teh_assert( args != NULL );
	teh_assert( args->length > 0 );
	teh_assert( args->length < 2 );
	// TODO: make it work
	
	tobj_print( stdout , args->arr_tobj[0] , false );
	
	return 0;
}
struct tobj * primitive_procedure_list( struct vector_tobj * args ) {
	teh_assert( args != NULL );
	teh_assert( args->length > 0);
	
	struct tobj * head = tobj_new_pair( args->arr_tobj[0] , 0 );
	struct tobj * ptr = head;
	for( size_t i = 1; i < args->length; ++i ) {
		ptr = tobj_pair_insert_after( ptr , args->arr_tobj[i] );
	}
	
	return head;
}
struct tobj * primitive_procedure_head( struct vector_tobj * args) {
	teh_assert( args != NULL );
	teh_assert( args->length == 1 );
	teh_assert( args->arr_tobj[0]->type == type_pair );
	return tobj_pair_get_head( tobj_pair_get_head(args->arr_tobj[0]) );
}
struct tobj * primitive_procedure_tail( struct vector_tobj * args) {
	teh_assert( args != NULL );
	teh_assert( args->length == 1 );
	teh_assert( args->arr_tobj[0]->type == type_pair );
	// TODO: make it work
	return tobj_pair_get_tail( tobj_pair_get_head(args->arr_tobj[0]) );
}
struct tobj * primitive_procedure_sum_int( struct vector_tobj * args ) {
	teh_assert( args != NULL );
	teh_assert( args->length > 0 );
	
	int result = 0;
	for( size_t i = 0; i < args->length; ++i ) {
		struct tobj *ptr = args->arr_tobj[i];
		teh_assert( ptr != NULL );
		result += tobj_get_int( ptr );
	}
	return tobj_new_int( result );
}
struct tobj * primitive_procedure_sum_double( struct vector_tobj * args ) {
	teh_assert( args != NULL );
	teh_assert( args->length > 0 );
	double result = 0;
	for( size_t i = 0; i < args->length; ++i ) {
		struct tobj *ptr = args->arr_tobj[i];
		teh_assert( ptr != NULL );
		result += tobj_get_double( ptr );
	}
	return tobj_new_double( result );
}
struct tobj * primitive_procedure_product_int( struct vector_tobj * args ) {
	teh_assert( args != NULL );
	teh_assert( args->length > 0 );
	
	int result = 1;
	for( size_t i = 0; i < args->length; ++i ) {
		struct tobj *ptr = args->arr_tobj[i];
		teh_assert( ptr != NULL );
		result *= tobj_get_int( ptr );
	}
	return tobj_new_int( result );
}
struct tobj * primitive_procedure_product_double( struct vector_tobj * args ) {
	teh_assert( args != NULL );
	double result = 1;
	for( size_t i = 0; i < args->length; ++i ) {
		struct tobj *ptr = args->arr_tobj[i];
		teh_assert( ptr != NULL );
		result *= tobj_get_double( ptr );
	}
	return tobj_new_double( result );
}
struct tobj * primitive_procedure_equal( struct vector_tobj * args ) {
	teh_assert( args != NULL );
	teh_assert( args->length > 0 );
	teh_assert( args->arr_tobj[0]->type != type_pair ); // cannot check equality of lists yet
	
	for( size_t i = 0; i < args->length; ++i ) {
		struct tobj *ptr = args->arr_tobj[i];
		teh_assert( ptr != NULL );
		if( !(tobj_are_equal( args->arr_tobj[0] , ptr)) ) {
			return f;
		}
	}
	return t;
}
struct tobj * primitive_procedure_list_length( struct vector_tobj * args ) {
	teh_assert( args != NULL );
	teh_assert( args->length == 1);
	
	int i = 0;
	struct tobj * ptr = args->arr_tobj[0];
	while( ptr != NULL ) {
		teh_assert( ptr->type == type_pair );
		++i;
		ptr = ptr->pair[1];
	}
	
	return tobj_new_int(i);
}
struct tobj * primitive_procedure_list_ref( struct vector_tobj * args ) {
	teh_assert( args != NULL );
	teh_assert( args->length == 1);
	/* TODO: this */
	
	struct tobj * ptr = args->arr_tobj[0];
	while( ptr != NULL ) {
		teh_assert( ptr->type == type_pair );
		ptr = ptr->pair[1];
	}
	
	return tobj_new_pointer( ptr );
}
struct tobj * primitive_procedure_setf( struct vector_tobj * args ) {
	teh_assert( args != NULL );
	teh_assert( args->length == 2 );
	struct tobj * ptr = args->arr_tobj[0];
	if( ptr->type == type_pointer ) {
		ptr = ptr->pointer;
	}
	
	struct tobj * copy = tobj_copy( args->arr_tobj[1] );
	*ptr = *copy;
	
	return 0;
}
struct tobj * primitive_procedure_push( struct vector_tobj * args ) {
	teh_assert( args != NULL );
	teh_assert( args->length == 1);
	
	env_stack_push( ENV , tobj_copy(args->arr_tobj[0]) );
	return 0;
}
struct tobj * primitive_procedure_pop( struct vector_tobj * args ) {
	teh_assert( args != NULL );
	teh_assert( args->length == 0);
	
	return env_stack_pop( ENV );
}
struct tobj * primitive_procedure_top( struct vector_tobj * args ) {
	teh_assert( args != NULL );
	teh_assert( args->length == 0);
	teh_assert( ENV->stack.length > 0 );
	
	return (ENV->stack.arr_tobj[ENV->stack.length-1]);
}
struct tobj * primitive_procedure_stack_ref( struct vector_tobj * args ) {
	teh_assert( args != NULL );
	teh_assert( args->length > 0);
	teh_assert( args->length < 2);
	teh_assert( ENV->stack.length > 0 );
	if( args->arr_tobj[0]->type != type_int ) {
		exception_with_tobj( args->arr_tobj[0] , "expected type %#x(type_int), got %#x" , type_int , args->arr_tobj[0]->type );
	}
	int const n = args->arr_tobj[0]->val_int;
	
	if( n < 0 ) {
		exception_with_tobj( args->arr_tobj[0] , "tried to get negative(-0x%x) stack reference" , -n );
	}
	
	if( (int) ENV->stack.length < n  ) {
		exception_with_tobj( args->arr_tobj[0] , "requested %#x element while stack has length of %#zx" , n , ENV->stack.length );
	}
	
	return tobj_new_pointer(ENV->stack.arr_tobj[ENV->stack.length - 1 - n]);
}
struct tobj * primitive_procedure_getf( struct vector_tobj * args ) {
	teh_assert( args != NULL );
	teh_assert( args->length > 0);
	teh_assert( args->length < 2);
	teh_assert( ENV->stack.length > 0 );
	if( args->arr_tobj[0]->type != type_pointer ) {
		exception_with_tobj( args->arr_tobj[0] , "expected type %#x(type_pointer), got %#x" , type_int , args->arr_tobj[0]->type );
	}
	
	return args->arr_tobj[0]->pointer;
}







static const struct primitive TABLE_PRIMITIVES[] = {
	{	"quote" ,
		0 ,
		primitive_procedure_quote } ,
	{	"print" ,
		0 ,
		primitive_procedure_print } ,
	{	"display" ,
		0 ,
		primitive_procedure_display } ,
	{	"list" ,
		0 ,
		primitive_procedure_list } ,
	{	"list-length" ,
		0 ,
		primitive_procedure_list_length } ,
	{	"eval" ,
		0 ,
		primitive_procedure_eval } ,
	{	"head" ,
		0 ,
		primitive_procedure_head } ,
	{	"tail" ,
		0 ,
		primitive_procedure_tail } ,
	{	"sum:int" ,
		0 ,
		primitive_procedure_sum_int ,} ,
	{	"sum:double" ,
		0 ,
		primitive_procedure_sum_double  } ,
	{	"product:int" ,
		0 ,
		primitive_procedure_product_int } ,
	{	"product:double" ,
		0 ,
		primitive_procedure_product_double } ,
	{	"equal?" ,
		0 ,
		primitive_procedure_equal  } ,
	{	"push" ,
		0 ,
		primitive_procedure_push } ,
	{	"pop" ,
		0 ,
		primitive_procedure_pop } ,
	{	"setf" ,
		0 ,
		primitive_procedure_setf } ,
	{	"top" ,
		0 ,
		primitive_procedure_top } ,
	{
		"stack-ref" ,
		0 ,
		primitive_procedure_stack_ref , } ,
	{
		"getf" ,
		0 ,
		primitive_procedure_getf , } ,
};


struct primitive const * match_primitive_by_symbol( char const * str )
{
	for( size_t i = 0; i < SIZEOF_ARRAY(TABLE_PRIMITIVES); ++i) {
		teh_assert( TABLE_PRIMITIVES[i].symbol != NULL );
		// teh_assert( TABLE_PRIMITIVES[i].proc_ptr != NULL );
		if( strcmp( TABLE_PRIMITIVES[i].symbol , str ) == 0 ) {
			return &(TABLE_PRIMITIVES[i]);
		}
	}
	
	exception( "failed to match primitive to symbol %s" , str );
}

/* } */







struct tobj * new_lambda( struct tobj * expr ) {
	// struct tobj * lambda = tobj_new( type_lambda );
	teh_assert( expr != NULL );
	teh_assert( expr->type == type_pair );
	teh_assert( expr->pair[0] != NULL );
	teh_assert( expr->pair[0]->type == type_pair );
	teh_assert( expr->pair[1] != NULL );
	teh_assert( expr->pair[1]->type == type_pair );
	
	return expr;
}



struct tobj * evaluate_lambda( struct tobj * lambda , struct tobj * tarr_args )
{
	tassert( lambda );
	tassert( lambda->type == type_lambda );
	tassert( lambda->lambda.body );
	tassert( tarr_args );
	tassert( tarr_args->type == type_arr_tobj );
	tassert( ((int)tarr_args->length) >= lambda->lambda.min_args );
	if( lambda->lambda.max_args > -1 ) {
		tassert( ((int)tarr_args->length) <= lambda->lambda.max_args );
	}
	
	
	return 0;
}






struct tobj * evaluate_list( struct tobj * expr ) {
/* list:
1. match symbol at position [0] to a primitive/procedure
2. build a vector of the rest
3. execute primitive/procedure
*/
	teh_assert( expr != NULL );
	teh_assert( expr->pair[0] != NULL );
	teh_assert( expr->pair[0]->arr_char != NULL );
	teh_assert( expr->pair[0]->type == type_char );
	teh_assert( expr->pair[0]->is_symbol );
	teh_assert( expr->pair[0]->length > 0 );
	if( expr->pair[0] == NULL ) {
		exception_with_tobj( expr , "cannot evaluate list with null car" );
	}
	if( !(expr->pair[0]->is_symbol) ) {
		exception_with_tobj( expr , "cannot evaluate list with head that isn't a symbol" );
	}
	
	const struct primitive * primitive = match_primitive_by_symbol( expr->pair[0]->arr_char );
	teh_assert( primitive->proc_ptr != NULL );
	
	/* special form for "quote" primitive: */
	if( primitive->proc_ptr == primitive_procedure_quote ) {
		struct vector_tobj * args = new_vector_tobj( 1 );
		struct tobj * obj = expr->pair[1];
		while( obj != NULL ) {
			vector_tobj_push( args , obj->pair[0] );
			obj = obj->pair[1];
		}
		return primitive->proc_ptr( args );
	}
	
	struct vector_tobj * args = new_vector_tobj( 1 ); // TODO: check for max_args while building `args` vector
	
	struct tobj * obj = expr->pair[1];
	while( obj != NULL ) {
		vector_tobj_push( args , evaluate(obj->pair[0]) );
		obj = obj->pair[1];
	}
	
	return primitive->proc_ptr( args );
}


struct tobj * evaluate_symbol( struct tobj * obj ) {
	teh_assert( obj != NULL );
	teh_assert( obj->type == type_char );
	teh_assert( obj->is_symbol );
	teh_assert( obj->capacity > 0 );
	teh_assert( obj->arr_char != NULL );
	
	if( isdigit(obj->arr_char[0])
	    || obj->arr_char[0] == '+'
	    || obj->arr_char[0] == '-' ) {
		/* TODO: add checking of whether the whole symbol was swallowed */
		char * ptr;
		if( strchr( obj->arr_char , '.' ) != NULL ) {
			double n = strtod( obj->arr_char , &ptr );
			return tobj_new_double( n );
		}
		long int n = strtol( obj->arr_char , &ptr , 0 );
		return tobj_new_int(n);
	}
	return tobj_new_symbol_copy_cstring( obj->arr_char );
}

struct tobj * evaluate( struct tobj * expr )
{
	// TODO: add the ability to evaluate numbers of type double
	teh_assert( expr != NULL );
	switch (expr->type) {
		case type_double:
		case type_int:
			return tobj_copy( expr );
		case type_char:
			if( expr->is_symbol ) {
				return evaluate_symbol( expr );
			} else {
				return tobj_copy( expr );
			}
		case type_pair: 
			return evaluate_list( expr );
		default:
			exception_with_tobj( expr , "evaluation: unexpected type %#x" , expr->type );
	}
	
	exception_with_tobj( expr , "failed to evaluate" );
}




