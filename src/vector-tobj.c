#include "vector-tobj.h"



void vector_tobj_initialize( struct vector_tobj * vect , size_t const capacity )
{
	teh_assert( vect != NULL );
	vect->arr_tobj = teh_calloc( capacity , sizeof(tobj_ptr) );
	teh_assert( vect->arr_tobj != NULL );
	vect->capacity = capacity;
	vect->length = 0;
}

struct vector_tobj * new_vector_tobj( size_t const capacity )
{
	struct vector_tobj *vect = teh_malloc( sizeof(struct vector_tobj) );
	teh_assert( vect != NULL );
	vect->capacity = capacity;
	vect->length = 0;
	vect->arr_tobj = teh_calloc( capacity , sizeof(tobj_ptr) );
	teh_assert( vect->arr_tobj != NULL );
	return vect;
}

void delete_vector_tobj( struct vector_tobj ** vect)
{
	teh_assert( vect != NULL );
	teh_assert( *vect != NULL );
	free( (*vect)->arr_tobj );
	free( *vect );
	*vect = NULL;
}

void vector_tobj_reserve( struct vector_tobj * vect , size_t const capacity )
{
	teh_assert( vect != NULL );
	teh_assert( capacity > 0 );
	vect->arr_tobj = teh_realloc( vect->arr_tobj , capacity * sizeof(struct tobj*) );
	vect->capacity = capacity; // I forgot about setting the capacity, and it gave me "realloc() invalid next size". I believe that it throws the error if trying to reallocate to the same size // NEW INFO: turns out that something else was corrupting the heap, so it's not because of wrong arguments
}

void vector_tobj_push( struct vector_tobj * vect , struct tobj * obj )
{
	teh_assert( vect != NULL );
	teh_assert( obj  != NULL );
	
	if( !(vect->capacity > vect->length +1 ) ) {
		vector_tobj_reserve( vect , 2 * vect->capacity );
	}
	
	vect->arr_tobj[vect->length] = obj;
	++(vect->length);
}

struct tobj * vector_tobj_pop( struct vector_tobj * vect )
{
	teh_assert( vect != NULL );
	teh_assert( vect->length > 0 );
	
	--(vect->length);
	return vect->arr_tobj[vect->length];
}

struct tobj ** vector_tobj_ref_top( struct vector_tobj * vect )
{
	teh_assert( vect != NULL );
	teh_assert( vect->length > 0 );
	
	return &(vect->arr_tobj[vect->length - 1]);
}

struct tobj * vector_tobj_peek( struct vector_tobj * vect )
{
	teh_assert( vect != NULL );
	teh_assert( vect->length > 0 );
	
	return vect->arr_tobj[vect->length - 1];
}

void vector_tobj_print_content( FILE * out , struct vector_tobj * vect )
{
	teh_assert( vect != NULL );
	for( size_t i = 0; i < vect->length; ++i ) {
		fprintf( out , "\n\t%#zx @%p: " , i , &vect->arr_tobj[i] );
		tobj_print( out , vect->arr_tobj[i] , true );
	}
}
