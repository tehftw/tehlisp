#include "exception.h"


jmp_buf exception_env;


void throw_exception( const char * file , int line , char * format , ... )
{
	fprintf( stderr , "exception in %s:%d " , file , line );
	
	if( format != NULL ) {
		va_list args;
		va_start( args , format );
		vfprintf( stderr , format , args );
		va_end(args);
		fputc( '\n' , stderr );
	}
	
	if( ON_EXCEPTION_EXIT_WITH_BACKTRACE ) {
		exit_with_backtrace( 0 );
	}
	
	
	longjmp( exception_env , 1 );
}


void exit_with_backtrace(int sig)
{
	void *array[0x10];
	size_t size;
	
	size = backtrace( array , 0x10 );
	
	fprintf( stderr , "exiting with signal %d" , sig );
	fprintf( stderr, "\nbacktrace:" );
	backtrace_symbols_fd(array , size , STDERR_FILENO);
	exit(1);
}


void teh_exception_with_backtrace( const char * file, int line, char * format , ... )
{
	fprintf( stderr ,  "\nteh exception in %s:%d: " , file , line );
	
	if( format != NULL ) {
		va_list args;
		va_start( args , format);
		vfprintf( stderr , format , args );
		va_end(args);
		fputc('\n' , stderr);
	}
	
	exit_with_backtrace( 0 );
}

