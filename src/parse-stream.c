#include "parse-stream.h"





struct tobj * get_symbol(
/* a new version to use with get_list */
		FILE * in , int const first_char , int * ch )
{
	struct tobj * symbol = tobj_new_arr_char( 0x10 );
	
	if( !is_allowed_in_symbol(first_char) ) {
		exception( "first character is not allowed in symbol(code: 0x%x '%c') " , first_char ,first_char );
	}
	
	for( *ch = first_char;
	     is_allowed_in_symbol(*ch) ;
	     *ch = fgetc(in) ) {
		tobj_arr_char_push( symbol , *ch );
	}
	
	if( symbol->length == 0 ) {
		tobj_delete( symbol );
		return NULL;
	}
	tobj_arr_char_push( symbol , '\0' );
	symbol->is_symbol = true;
	return symbol;
}


struct tobj * get_string( FILE * in , int * last_char_pointer )
{
	// TODO: add a way to "rewind" the stream
	// , or to pass the character that was last readed( first after symbol )
	struct tobj * str = tobj_new_arr_char( 0x10 );
	
	bool delimited = false;
	for( int ch = fgetc(in);
	     ch != EOF;
	     ch = fgetc(in) ) {
		if( delimited ) {
			switch( ch ) {
				case '\n': break;
				case 'n': tobj_arr_char_push( str , '\n' ); break;
				case 't': tobj_arr_char_push( str , '\t' ); break;
				default : tobj_arr_char_push( str , ch );
			}
			delimited = false;
		} else {
			switch( ch ) {
				case '\\': delimited = false; break;
				case '"': *last_char_pointer = ch; goto finish;
				default: tobj_arr_char_push( str , ch );
			}
		}
	}
	
	exception_with_tobj( str , "EOF before string-ending delimiter" );
	
finish:
	tobj_arr_char_push( str , '\0' );
	return str;
}


struct tobj * get_list(
		FILE * in  )
{
	struct tobj * list = tobj_new_pair( 0 , 0 );
	struct tobj * ptr = list;
	
	for( int ch = fgetc(in);
	     ch != EOF;
	     ch = fgetc(in) ) { switch( ch ) {
			case '(':
				ptr = tobj_pair_insert_after( ptr , get_list( in )  );
				break;
			case ')':
				goto finish;
			case '"':
				ptr = tobj_pair_insert_after( ptr , get_string( in , &ch ) );
				break;
			default:
				if( is_separator(ch) ) {
					break;
				}
				if( is_allowed_in_symbol(ch) ) {
					ptr = tobj_pair_insert_after( ptr , get_symbol( in , ch , &ch ) );
					if( ch == ')' ) {
						goto finish;
					}
				}
		}
	}
	
	exception_with_tobj( list , "unexpected EOF while reading a list. expected list to end with ')'" );
	
finish:
	if( list->pair[0] == NULL ) {
		tassert( list->pair[1]  == NULL );
		free( list );
		return NULL;
	}
	return list;
}




struct tobj * parse_stream_recursive_2( FILE * in )
{
	if( in == NULL ) {
		exception( "tried to parse null FILE* " );
	}
	
	
	struct tobj * list = tobj_new_pair( 0 , 0 );
	struct tobj * ptr = list;
	
	
	bool comment_mode = false;
	
	for( int ch = fgetc(in);
	     ch != EOF;
	     ch = fgetc(in) ) {
		if( comment_mode ) {
			if( ch == '\n' ) {
				comment_mode = false;
			}
		} else { switch(ch) {
		case '(':
			ptr = tobj_pair_insert_after( ptr , get_list(in) );
			break;
		case ')':
			exception( "unexpected ending-delimiter '%c' while searching for a readable object" , ch );
		case '/':
			comment_mode = true;
			break;
		case '\n':
		case ' ' :
		case '\t':
		case ';' :
		case ',' :
			break;
		default:
			exception( "unexpected character 0x%x ('%c') while looking for a list" , ch , ch );
		}}
	}
	
	
	if( list->pair[0] == NULL ) {
		exception( "parsed a stream without content" );
	}
	return list;
}
