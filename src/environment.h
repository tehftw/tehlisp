#pragma once


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <setjmp.h>
#include <string.h>
#include <stdarg.h>
#include <stdbool.h>
#include <ctype.h>


#include "exception.h"
#include "teh-alloc.h"
#include "macros.h"
#include "tobj.h"
#include "vector-tobj.h"
#include "parse-stream.h"







struct env {
	struct vector_tobj stack;
};

extern struct env * ENV; // global environment


void env_initialize(
		 struct env * env
		,size_t const stack_capacity );
void env_stack_push(
		 struct env * env
		,struct tobj * obj );
struct tobj * env_stack_pop(
		struct env * env );
void env_stack_print(
		 FILE * out 
		,struct env * env );
