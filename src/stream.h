#pragma once


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include <setjmp.h>

#include "teh-alloc.h"
#include "tobj.h"
#include "tstring.h"

typedef struct istream Istream;
struct istream {
	char * name;
	FILE * file;
	int line;
	int last_char;
};


const char * TABLE_CHARACTERS_ENDING_CHARACTERS;
const char *TABLE_CHARACTERS_ALLOWED_IN_SYMBOL;
const char *TABLE_CHARACTERS_SEPARATORS;

bool is_separator( int const ch );
bool is_allowed_in_symbol( int const ch);
bool is_symbol_ending( int const ch );


// Istream * istream_stdin;


Istream * istream_open_file(
/* doesn't copy path */
		char * path );
void istream_close(
		Istream * is );
int istream_getchar(
		Istream * is );
void istream_print_info(
		FILE * out ,
		const Istream * is );



struct tobj * istream_get_string( Istream * is );
struct tobj * istream_get_symbol( Istream * is );

struct tobj * istream_parse_stream_recursive( Istream * is);
