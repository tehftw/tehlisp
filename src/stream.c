#include "stream.h"


const char * TABLE_CHARACTERS_ENDING_CHARACTERS = " \t\n;,/\\()[]{}'\"`";
const char * TABLE_CHARACTERS_ALLOWED_IN_SYMBOL = "!#$%&*+-.:=?@^_~";
const char * TABLE_CHARACTERS_SEPARATORS = " \t\n;,";

bool is_separator( int const ch ) {
	return (strchr( TABLE_CHARACTERS_SEPARATORS , ch ) != NULL);
}

bool is_allowed_in_symbol( int const ch) {
	return (isalnum(ch))
	    || (strchr(TABLE_CHARACTERS_ALLOWED_IN_SYMBOL, ch) != NULL);
}

bool is_symbol_ending( int const ch ) {
	return (ch == EOF)
		|| (strchr( TABLE_CHARACTERS_ENDING_CHARACTERS , ch ) != NULL);
}





Istream * istream_open_file(
		char * path )
{
	Istream * is = teh_calloc( 1 , sizeof( Istream) );
	is->file = fopen( path , "r" );
	is->line = 0;
	is->name = path;
	return is;
}

void istream_close(
		Istream * is )
{
	tassert( is );
	fclose( is->file );
	free( is );
}

void istream_print_info(
		FILE * out ,
		const Istream * is )
{
	teh_assert( is );
	fprintf( out , "%s:%d" , is->name , is->line);
}

int istream_getchar(
		 Istream * is )
{
	int ch = fgetc( is->file );
	if( ch == '\n' ) {
		++(is->line);
	}
	is->last_char = ch;
	return ch;
}




struct tobj * istream_get_string( Istream * is ) {
	teh_assert( is );
	if( is->last_char != '\"' ) {
		istream_print_info( stderr , is );
		exception( "last_char while reading a string should be ('\"' code 0x%x), but it is: (code: 0x%x '%c') " , '"' , is->last_char , is->last_char );
	}
	struct tobj * str = tobj_new_arr_char( 0x10 );
	
	
	bool is_delimited = false;
	for( int ch = istream_getchar(is);
	     ch != EOF;
	     ch = istream_getchar(is) ) {
		if( is_delimited ) {
			switch( ch ) {
				case '\n': break;
				case 'n': tobj_arr_char_push( str , '\n' ); break;
				case 't': tobj_arr_char_push( str , '\t' ); break;
				default : tobj_arr_char_push( str , ch );
			}
			is_delimited = false;
		} else {
			switch( ch ) {
				case '\\': is_delimited = false; break;
				case '"': goto finish;
				default: tobj_arr_char_push( str , ch );
			}
		}
	}
	
	istream_print_info( stderr , is );
	exception_with_tobj( str , "EOF before string-ending delimiter" );
	
finish:
	tobj_arr_char_push( str , '\0' );
	return str;
	
	
	if( str->length == 0 ) {
		tobj_delete( str );
		return NULL;
	}
	tobj_arr_char_push( str , '\0' );
	str->is_symbol = false;
	return str;
}


struct tobj * istream_get_symbol( Istream * is )
{
	teh_assert( is );
	if( !is_allowed_in_symbol(is->last_char) ) {
		istream_print_info( stderr , is );
		exception( "first character is not allowed in symbol(code: 0x%x '%c') " , is->last_char , is->last_char );
	}
	struct tobj * symbol = tobj_new_arr_char( 0x10 );
	
	for( int ch = is->last_char;
	     is_allowed_in_symbol( ch ) ;
	     ch = istream_getchar( is ) ) {
		tobj_arr_char_push( symbol , ch );
	}
	
	if( symbol->length == 0 ) {
		tobj_delete( symbol );
		return NULL;
	}
	
	
	tobj_arr_char_push( symbol , '\0' );
	symbol->is_symbol = true;
	
	if( symbol->arr_char[0] == '+'
	 || symbol->arr_char[0] == '-'
	 || isdigit(symbol->arr_char[0]) ) {
		char * ptr;
		if( strchr(symbol->arr_char , '.' )  ) {
			double val_double = strtod( symbol->arr_char , &ptr );
			tobj_delete( symbol );
			tobj_new_double( val_double );
		} else {
			int val_int = strtol( symbol->arr_char , &ptr , 0 );
			tobj_delete( symbol );
			tobj_new_int( val_int );
		}
		if( *ptr != '\0' ) {
			tobj_delete( symbol );
			istream_print_info( stderr , is );
			exception( "while parsing as number, didn't swallow the whole symbol" );
		}
	}
	
	
	return symbol;
}




struct tobj * istream_get_list(
		Istream * is  )
{
	struct tobj * list = tobj_new_pair( 0 , 0 );
	struct tobj * ptr = list;
	
	for( int ch = istream_getchar( is );
	     ch != EOF;
	     ch = istream_getchar( is ) ) { switch( ch ) {
			case '(':
				ptr = tobj_pair_insert_after( ptr , istream_get_list( is )  );
				break;
			case ')':
				goto finish;
			case '"':
				ptr = tobj_pair_insert_after( ptr , istream_get_string( is ) );
				break;
			default:
				if( is_separator(ch) ) {
					break;
				}
				if( is_allowed_in_symbol(ch) ) {
					ptr = tobj_pair_insert_after( ptr , istream_get_symbol( is ) );
					if( is->last_char == ')' ) {
						goto finish;
					}
				}
		}
	}
	
	exception_with_tobj( list , "unexpected EOF while reading a list. expected list to end with ')'" );
	
finish:
	if( list->pair[0] == NULL ) {
		tassert( list->pair[1]  == NULL );
		free( list );
		return NULL;
	}
	return list;
}




struct tobj * istream_parse_stream_recursive( Istream * is)
{
	teh_assert( is );
	if( !is ) {
		exception( "\ntried to parse null Istream* " );
	}
	
	
	struct tobj * list = tobj_new_pair( 0 , 0 );
	struct tobj * ptr = list;
	
	
	bool comment_mode = false;
	
	for( int ch = istream_getchar(is);
	     ch != EOF;
	     ch = istream_getchar(is) ) {
		if( comment_mode ) {
			if( ch == '\n' ) {
				comment_mode = false;
			}
		} else { switch(ch) {
		case '(':
			ptr = tobj_pair_insert_after( ptr , istream_get_list( is ) );
			break;
		case ')':
			exception( "unexpected ending-delimiter '%c' while searching for a readable object" , ch );
		case '/':
			comment_mode = true;
			break;
		case '\n':
		case ' ' :
		case '\t':
		case ';' :
		case ',' :
			break;
		default:
			exception( "unexpected character 0x%x ('%c') while looking for a list" , ch , ch );
		}}
	}
	
	
	if( list->pair[0] == NULL ) {
		exception( "parsed a stream without content" );
	}
	return list;
}
