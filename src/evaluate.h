#pragma once


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <setjmp.h>
#include <string.h>
#include <stdarg.h>
#include <stdbool.h>
#include <ctype.h>


#include "exception.h"
#include "teh-alloc.h"
#include "macros.h"
#include "tobj.h"
#include "vector-tobj.h"
#include "environment.h"

typedef struct tobj * primitive_procedure (struct vector_tobj *);


enum primitive_code {
	primitive_quote
};

struct primitive {
	char * symbol;
	enum primitive_code prim_code;
	primitive_procedure * proc_ptr;
	
	//size_t min_args;
	//size_t max_args;
};



struct tobj * evaluate( struct tobj * expr );

