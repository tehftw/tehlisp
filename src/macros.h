#pragma once


#define SIZEOF_ARRAY(arr) ((sizeof(arr))/(sizeof(arr[0])))

#define tassert( predicate ) teh_assert( predicate )

#define SWAP_VARIABLES( a , b ) do { \
		typeof(a) tmp = a; \
		a = b; \
		b = tmp; \
	} while(0)

#define STRINGIFY( x ) #x


/*#define teh_assert( predicate ) do {  \
	if( !(predicate) ) { \
		fprintf(stderr, "%s:%d teh-assert(%s) failed. inducing segmentation fault by dereferencing a nullpointer...\n" ,  __FILE__ , __LINE__, #predicate ); \
		int *n = NULL; \
		*n = 1; \
	} } while(0)*/

