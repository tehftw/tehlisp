#include "tobj.h"

// TODO: tobj_autoset_pointer(), to make it easier to copy/create tobjs


struct tobj * f = &(struct tobj){ type_special , .val_char = "f" };
struct tobj * t = &(struct tobj){ type_special , .val_char = "t" };



bool is_valid_type( enum type const type ) {
	return (type >= type_double && type < TYPES_COUNT);
}

const size_t TABLE_VECTOR_SIZE_OF_MEMBER[] = {
	[type_arr_char] = sizeof( char ) ,
	[type_arr_tobj] = sizeof( struct tobj * ) ,
};


size_t get_size_of_member( enum type const type )
{
	assert( type < TYPES_COUNT );
	return TABLE_VECTOR_SIZE_OF_MEMBER[type];
}


bool tobj_has_array( const struct tobj * const obj )
{
	tassert( obj );
	return (obj->ptr);
}


void exception_with_tobj( struct tobj const * tobj , char * format , ... )
{
	if( tobj != NULL ) {
		fputs( "(object :" , stderr );
		tobj_print( stderr , tobj , true );
		fputs( ") " , stderr );
	}
	
	va_list args;
	va_start( args , format );
	vfprintf( stderr , format , args );
	va_end(args);
	fputc( '\n' , stderr );
	teh_assert( !ON_EXCEPTION_EXIT_WITH_BACKTRACE );
	
	
	longjmp( exception_env , 1 );
}




void tobj_print( FILE * out , struct tobj const * tobj , bool const readably )
{
	if( tobj == NULL ) {
		fputs( "<null>" , out);
		return;
	}
	if( tobj == f || tobj == t ) {
		fputs( tobj->val_char , out );
		return;
	}
	
	
	switch( tobj->type ) {
#define CASE(type, ...)             \
	case type:                      \
		fprintf(out, __VA_ARGS__); \
		break
		CASE( type_double , "%g" , tobj->val_double  );
		CASE( type_int , "%d" , tobj->val_int );
		CASE( type_pointer , "<pointer to %p>" , tobj->pointer );
		CASE( type_special , "<special>");
		CASE( type_lambda , "<lambda>" );
		CASE( type_primitive , "<primitive>" );
#undef CASE
	case type_arr_char:
		if( tobj->is_symbol ) {
			fputc( '\'' , out );
			fputs( tobj->arr_char , out);
		} else if (readably){
			fputs( tobj->arr_char , out );
		} else {
			fputc( '"' , out );
			for( size_t i = 0; i < tobj->length; ++i ) {
				switch( tobj->arr_char[i] ) {
					case '"':  fputs("\\\"",  out ); break;
					case '\t': fputs("\\t",   out ); break;
					case '\r': fputs("\\r",   out ); break;
					case '\n': fputs("\\n",   out ); break;
					case '\\': fputs("\\\\",  out ); break;
					default:   fputc( tobj->arr_char[i] , out ); break;
				}
			}
			fputc( '"' , out );
		}
		break;
	case type_pair:
		fputc( '(' , out );
		tobj_print( out , tobj->pair[0] , readably );
		while( tobj->pair[1]!= NULL ) {
			tobj = tobj->pair[1];
			if( tobj->type == type_pair ) {
				fputc( ' ' , out);
				tobj_print( out , tobj->pair[0] , readably );
			} else {
				fputs( " . " , out );
				tobj_print( out , tobj , readably );
				break;
			}
		}
		fputc( ')' , out );
		break;
	default:
		exception( "tried to print unrecognized type %#x (tobj @%p)" , tobj->type , tobj );
	}
}




void tobj_print_2( FILE * out , struct tobj const * tobj , bool const readably )
{
	if( tobj == NULL ) {
		fputs( "<null>" , out);
		return;
	}
	
	
	if( tobj_is_array(tobj) ) {
		switch( tobj->type ) {
			case type_pointer:
				fputc( '[' , out );
				fputc( ' ' , out );
				tobj_print_2( out , tobj->pointer , readably );
				for( size_t i = 0; i < tobj->length; ++i ) {
					fputc( ' ' , out );
					tobj_print_2( out , tobj->pointer , readably );
				}
				fputc( ']' , out );
				break;
			default:
				exception( "tried to print unrecognized array-type %#x (tobj @%p)" , tobj->type , tobj );
		}
		return;
	}
	
	
	switch( tobj->type ) {
		case type_double: fprintf( out ,  "%g" , tobj->val_double ); break;
		case type_int: fprintf( out , "%d" , tobj->val_int  ); break;
		case type_char:
			if( tobj->is_symbol ) {
				fprintf( out , "'%s" , tobj->arr_char   );
			}
			if( readably ) {
				fputc( '"' , out );
				for( char const * str = tobj->arr_char; *str != '\0'; ++str  ) {
					switch( *str ) {
						case '"':  fputs("\\\"",  out ); break;
						case '\t': fputs("\\t",   out ); break;
						case '\r': fputs("\\r",   out ); break;
						case '\n': fputs("\\n",   out ); break;
						case '\\': fputs("\\\\",  out ); break;
						default:   fputc( *str , out ); break;
					}
				}
				fputc( '"' , out );
			} else {
				fputs( tobj->arr_char , out );
			}
			break;
		case type_pair:
			fputc( '(' , out );
			tobj_print( out , tobj->pair[0] , readably );
			while( tobj->pair[1]!= NULL ) {
				tobj = tobj->pair[1];
				if( tobj->type == type_pair ) {
					fputc( ' ' , out);
					tobj_print( out , tobj->pair[0] , readably );
				} else {
					fputs( " . " , out );
					tobj_print( out , tobj , readably );
					break;
				}
			}
			fputc( ')' , out );
			break;
		case type_pointer:
			fprintf( out , "<pointer: %p>" , tobj->pointer );
			break;
		default:
			exception( "tried to print unrecognized type %#x (tobj @%p)" , tobj->type , tobj );
	}
}











bool tobj_is_array( struct tobj const * const obj )
{
	assert( obj != NULL );
	return ((obj->capacity > 0) && (obj->ptr != NULL));
}


void tobj_autoset_pointer( struct tobj * obj )
{
	tassert( obj );
	if( !(obj->ptr) ) {
		return;
	}
	
	switch( obj->type ) {
		case type_arr_char:
			obj->arr_char = obj->ptr;
			break;
		case type_arr_tobj:
			obj->arr_tobj = obj->ptr;
			break;
		default:
			exception_with_tobj( obj , "tried to tobj_autoset_pointer object of unexpected type: %#x" , obj->type );
	}
	
}


struct tobj * tobj_new( enum type const type )
{
	struct tobj * obj = teh_malloc( sizeof(struct tobj) );
	obj->type = type;
	obj->length = 0;
	obj->capacity = 0;
	obj->ptr = NULL;
	obj->void_ptr = NULL;
	return obj;
}


static struct tobj * tobj_new_with_size( enum type const type , size_t const size )
{
	struct tobj * obj = tobj_new( type );
	obj->ptr = teh_malloc( size );
	teh_assert( obj->ptr != NULL );
	obj->void_ptr = obj->ptr;
	return obj;
}


struct tobj * tobj_new_with_array( enum type const type , size_t const nmemb )
{
	teh_assert( TABLE_VECTOR_SIZE_OF_MEMBER[type] > 0 );
	void * ptr = teh_calloc( TABLE_VECTOR_SIZE_OF_MEMBER[type] , nmemb );
	teh_assert( ptr != NULL );
	struct tobj * obj = tobj_new( type );
	tassert( obj != NULL );
	obj->ptr = ptr;
	obj->void_ptr = ptr;
	obj->capacity = nmemb;
	return obj;
}




void tobj_resize_array( struct tobj * obj , size_t const capacity )
{
	teh_assert( obj != NULL );
	teh_assert( obj->ptr != NULL );
	teh_assert( TABLE_VECTOR_SIZE_OF_MEMBER[obj->type] > 0 );
	obj->ptr = realloc( obj->ptr , capacity * TABLE_VECTOR_SIZE_OF_MEMBER[obj->type] );
	obj->capacity = capacity;
}


void tobj_autogrow_array( struct tobj * obj )
{
	tassert( obj );
	tassert( obj->capacity > 0 );
	if( !(obj->capacity > obj->length) ) {
		tobj_resize_array( obj , 2 * (obj->capacity) );
	}
}

void tobj_resize_array_to_fit( struct tobj * obj )
{
	tassert( obj != NULL );
	tassert( obj->capacity > 0 );
	tassert( obj->capacity > obj->length );
	if( obj->length < obj->capacity ) {
		tobj_resize_array( obj , obj->length );
	}
}




void tobj_delete( struct tobj * obj )
{
	if(  obj == NULL
	  || obj == t
	  || obj == f
	) {
		return;
	}
	if( obj->type == type_pair ) {
		tobj_delete( obj->pair[0] );
		tobj_delete( obj->pair[1] );
	}
	if( obj->ptr != NULL ) {
		free( obj->ptr );
	}
	free( obj );
}









struct tobj * tobj_new_arr_char( size_t const size )
{
	struct tobj * v = tobj_new_with_size( type_char , size + 1 );
	v->arr_char = v->ptr;
	return v;
}
struct tobj * tobj_new_arr_char_copy_cstring( const char * cstr  )
{
	size_t const len = strlen( cstr );
	struct tobj * obj = tobj_new_arr_char( len + 1 );
	strncpy( obj->arr_char , cstr , len + 1 );
	obj->arr_char[obj->capacity - 1] = '\0';
	return obj;
}
void tobj_arr_char_push( struct tobj * obj ,  const char c )
{
	tassert( obj != NULL );
	tassert( obj->type == type_char );
	
	if( obj->capacity == 0 ) {
		tobj_resize_array( obj , 0x10 );
	}
	
	teh_assert( obj->capacity > 0 );
	if( !( obj->capacity > obj->length ) ) {
		tobj_autogrow_array( obj );
	}
	
	obj->arr_char[obj->length] = c;
	++(obj->length);
}


struct tobj * tobj_new_symbol_copy_cstring( const char * cstr  ) {
	struct tobj * obj = tobj_new_arr_char_copy_cstring( cstr );
	obj->is_symbol = true;
	return obj;
}


struct tobj * tobj_new_arr_tobj( size_t const capacity )
{
	struct tobj * arr = tobj_new_with_array( type_arr_tobj , capacity );
	arr->arr_tobj = arr->ptr;
	return arr;
}


struct tobj * tobj_new_pointer( struct tobj * ptr)
{
	struct tobj * obj = tobj_new(type_pointer);
	obj->pointer = ptr;
	return obj;
}


struct tobj * tobj_new_double( const double val )
{
	struct tobj * obj = tobj_new(type_double);
	obj->val_double = val;
	return obj;
}


struct tobj * tobj_new_int( const int val )
{
	struct tobj * obj = tobj_new(type_int);
	obj->val_int = val;
	return obj;
}


struct tobj * tobj_new_pair( struct tobj * ptr0 , struct tobj * ptr1 )
{
	struct tobj * obj = tobj_new(type_pair);
	obj->pair[0] = ptr0;
	obj->pair[1] = ptr1;
	return obj;
}


struct tobj * tobj_pair_get_head(struct tobj * obj)
{
	assert( obj != NULL );
	assert( obj->type ==  type_pair);
	return obj->pair[0];
}
struct tobj * tobj_pair_get_tail(struct tobj * obj)
{
	assert( obj != NULL );
	assert( obj->type ==  type_pair);
	return obj->pair[1];
}
struct tobj * tobj_pair_insert_after(
/* returns pointer to the pair containing next */
		struct tobj * list , struct tobj * obj  )
{
	if( list == NULL  ) {
		return tobj_new_pair( obj , 0 );
	}
	if( list->type != type_pair ) {
		exception( "tried to insert after non-pair object: @%p of type %#x(expected %#x)" , list , list->type , type_pair );
	}
	if( list->pair[0] == NULL  ) {
		list->pair[0] = obj;
		return list;
	}
	list->pair[1] = tobj_new_pair( obj , 0);
	return list->pair[1];
}
struct tobj * tobj_pair_insert_after_1(
/* returns pointer to the pair containing next */
		struct tobj * list , struct tobj * obj  )
{
	if( list == NULL  ) {
		return tobj_new_pair( obj , 0 );
	}
	if( list->type != type_pair ) {
		exception( "tried to insert after non-pair object: @%p of type %#x(expected %#x)" , list , list->type , type_pair );
	}
	list->pair[1] = tobj_new_pair( obj , 0);
	return list->pair[1];
}

void tobj_pair_extend(
/*
   if (*ref == NULL): constructs a list with obj as car
   if (*ref->type == type_pair): appends obj as new cell, after *ref
*/
		 struct tobj ** ref
		,struct tobj * obj  )
{
	assert( ref != NULL );
	
	if( (*ref) == NULL ) {
		(*ref) = tobj_new_pair( obj , 0 );
		return;
	}
	if( (*ref)->type == type_pair ) {
		(*ref)->pair[1] = tobj_new_pair( obj , 0 );
		(*ref) = (*ref)->pair[1];
	}
	
	exception( "tried to insert after non-pair object: @%p of type %#x(expected %#x)" 
			, (*ref) , (*ref)->type , type_pair );
}


struct tobj * tobj_copy( const struct tobj * obj )
{
	assert( obj != NULL );
	assert( is_valid_type(obj->type) );
	
	struct tobj * copy;
	void * ptr = 0;
	if( get_size_of_member(obj->type) > 0 ) {
		copy = tobj_new_with_array( obj->type , obj->capacity );
		ptr = copy->ptr;
	} else {
		copy = tobj_new( obj->type );
	}
	
	memcpy( copy , obj , sizeof(struct tobj) );
	
	if( ptr ) {
		memcpy( ptr , obj->ptr , ( get_size_of_member(obj->type) * obj->capacity) );
		copy->ptr = ptr;
		tobj_autoset_pointer( copy );
		if( (obj->type != type_arr_char) ) {
			exception( "tried to copy array of type %#x, only type_arr_char is supported yet" , obj->type );
		}
	}
	
	
	
	return copy;
}


int tobj_get_int( struct tobj * obj )
{
	assert( obj != NULL );
	switch( obj->type ) {
		case type_int:
			return obj->val_int;
		case type_double:
			return (int)obj->val_double;
		case type_pointer:
			return tobj_get_int( obj->pointer );
		default:
			exception_with_tobj( obj , "cannot get 'int'-type number from a non-number object ");
	}
}
double tobj_get_double( struct tobj * obj )
{
	assert( obj != NULL );
	
	switch( obj->type ) {
		case type_int:
			return (double)obj->val_int;
		case type_double:
			return obj->val_double;
		case type_pointer:
			return tobj_get_double( obj->pointer );
		default:
			exception_with_tobj( obj , "cannot get 'double'-type value from a non-number object ");
	}
}


bool tobj_are_equal( struct tobj * t_0 , struct tobj * t_1 ) {
	if( t_0 == t_1  ) {
		return true;
	}
	assert( t_0 != NULL );
	assert( t_1 != NULL );
	
	if( t_0->type != t_1->type  ) {
		return false;
	}
	if( t_0->capacity != t_1->capacity ) {
		return false;
	}
	if( t_0->type == type_pair ) {
		exception_with_tobj( t_0 , "equality-checking lists doesn't work yet" );
	}
	
	switch(t_0->type) {
		case type_double:
			return (t_0->val_double == t_1->val_double);
			break;
		case type_int:
			return (t_0->val_int == t_1->val_int);
			break;
		case type_char:
			return (strcmp( t_0->arr_char , t_1->arr_char ) == 0);
			break;
		case type_pair:
			exception_with_tobj( t_0 , "equality-checking lists doesn't work yet" );
			break;
		default:
			exception_with_tobj( t_0 , "unrecognized type %#x while checking for equality" , t_0->type );
	}
}




size_t tobj_list_length( struct tobj const * const head )
{
	teh_assert( head != NULL );
	size_t i = 0;
	for( struct tobj const * ptr = head;
			ptr != NULL;
			ptr = ptr->pair[1] ) {
		teh_assert( ptr->type == type_pair );
		++i;
	}
	return i;
}











