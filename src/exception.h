#pragma once



#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <setjmp.h>
#include <string.h>
#include <stdarg.h>
#include <stdbool.h>
#include <ctype.h>
#include <unistd.h>
#include <signal.h>
#include <execinfo.h>


#define ON_EXCEPTION_EXIT_WITH_BACKTRACE true

extern jmp_buf exception_env;




#ifdef __GNUC__
void exception( char *format , ... )
  __attribute__ ((noreturn, format(printf, 1, 2)));
#endif


void exit_with_backtrace(int) __attribute__ ((noreturn));
void teh_exception_with_backtrace( const char * file, int line, char * format , ... );

#define teh_assert( predicate ) \
	do { if( !(predicate) ) {		\
		teh_exception_with_backtrace( __FILE__ ,  __LINE__ , "assertion failed: %s" , #predicate );		\
	}} while(0)


void throw_exception( const char * file , int line , char * format , ... ) 
	__attribute__ ((noreturn , format(printf , 3 , 4)));

#define exception( format , ... ) do { \
		throw_exception( __FILE__ , __LINE__ , format , ##__VA_ARGS__); \
	} while(0)

