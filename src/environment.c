#include "environment.h"


struct env * ENV; // global environment


void env_initialize( struct env * env , size_t const stack_capacity )
{
	assert( env != NULL );
	vector_tobj_initialize(  &(env->stack)  , stack_capacity );
}

void env_stack_push( struct env * env , struct tobj * obj)
{
	assert( env != NULL);
	assert( obj != NULL );
	vector_tobj_push( &(env->stack) , obj );
}

struct tobj * env_stack_pop( struct env * env )
{
	assert( env != NULL);
	return vector_tobj_pop( &(env->stack) );
}

void env_stack_print( FILE * out ,  struct env * env )
{
	fprintf( out , "\nenv stack (%#zx):\n" , env->stack.length );
	for( size_t i = 0 , j = (env->stack.length - 1); i < env->stack.length; ++i , --j ) {
		fprintf( out , "0x%zx: " , j );
		tobj_print( out , env->stack.arr_tobj[j] , false  );
		fprintf( out ,  "\n" );
	}
}
