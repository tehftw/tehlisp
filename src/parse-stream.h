#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <setjmp.h>
#include <string.h>
#include <stdarg.h>
#include <stdbool.h>
#include <ctype.h>

#include "tobj.h"
#include "vector-tobj.h"
#include "stream.h"

struct tobj * parse_file( char * const filepath );
struct tobj * parse_stream_recursive( FILE * in );
struct tobj * parse_stream_1( FILE * in );
struct tobj * read_string( FILE * in );



struct tobj * parse_stream_recursive_2( FILE * in );
