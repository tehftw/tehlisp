#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <setjmp.h>
#include <string.h>
#include <stdarg.h>
#include <stdbool.h>
#include <ctype.h>

#include "macros.h"
#include "exception.h"


void * teh_malloc( size_t const size ) __attribute__ ((malloc)) ;
void * teh_calloc( size_t const s0 , size_t const s1 ) __attribute__ ((malloc)) ;
void * teh_realloc( void * ptr_r, size_t const size ) __attribute__ ((malloc)) ;
