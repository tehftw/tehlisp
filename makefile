


bin/clang-teh-lisp:
	clang -Wall -Wextra -g -Og -rdynamic -lSegFault src/*.c -o bin/clang-teh-lisp

bin/gcc-teh-lisp:
	gcc -Werror -fmax-errors=1 -Wall -Wextra -g -Og -rdynamic -lSegFault src/*.c -o bin/gcc-teh-lisp
# -rdynamic : to show callstack upon segfault(check handler() in main.c)


clang: bin/clang-teh-lisp

gcc: bin/gcc-teh-lisp

clean:
	$(RM) bin/*


